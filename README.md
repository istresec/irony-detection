# Irony Detection

A system for detecting irony in Twitter posts.

Made for the Text Analysis and Retrieval (TAR) course at FER, UniZG, academic year 2020/2021.